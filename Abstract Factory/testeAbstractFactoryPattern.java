import design.pattern.creational.objects.FrenchAddress;
import desing.pattern.creational.factory.AddressFactory;
import desing.pattern.creational.factory.FrenchAddressFactory;

public class testeAbstractFactoryPattern {

	public static void main(String[] args) {
		AddressFactory frenchAddressFactory = new FrenchAddressFactory();
		FrenchAddress frenchAddress = (FrenchAddress) frenchAddressFactory.createAddress();
		frenchAddress.setCity("Paris");
		frenchAddress.setPostalCode("12412412");
		frenchAddress.setStreet("Street");
		System.out.println(frenchAddress.getFullAddress());
		
	}	
}
