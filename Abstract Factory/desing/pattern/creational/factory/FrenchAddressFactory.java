package desing.pattern.creational.factory;

import design.pattern.creational.objects.FrenchAddress;
import design.pattern.creational.objects.FrenchPhoneNumber;
import desing.pattern.creational.entity.Address;
import desing.pattern.creational.entity.PhoneNumber;

public class FrenchAddressFactory implements AddressFactory {

	@Override
	public Address createAddress() {
		return new FrenchAddress();
	}

	@Override
	public PhoneNumber createPhoneNumber() {
		return new FrenchPhoneNumber();
	}

}
