package desing.pattern.creational.factory;

import desing.pattern.creational.entity.Address;
import desing.pattern.creational.entity.PhoneNumber;

public interface AddressFactory {
	public Address createAddress();
	public PhoneNumber createPhoneNumber();
}


/*Note that the AddressFactory defines two factory methods, createAddress and createPhoneNumber. The methods produce
the abstract products Address and PhoneNumber, which define methods that these products support. */