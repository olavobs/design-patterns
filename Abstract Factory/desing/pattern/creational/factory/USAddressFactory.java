package desing.pattern.creational.factory;

import design.pattern.creational.objects.USAddress;
import design.pattern.creational.objects.USPhoneNumber;
import desing.pattern.creational.entity.Address;
import desing.pattern.creational.entity.PhoneNumber;

public class USAddressFactory implements AddressFactory{

	@Override
	public Address createAddress() {
		return new USAddress();
	}

	@Override
	public PhoneNumber createPhoneNumber() {
		return new USPhoneNumber();
	}

}
