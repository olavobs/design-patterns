Purpose

To simplify complex object creation by defining a class whose purpose is to build instances of another class. The Builder produces one main product, such that there might be more than one class in the product, but there is always one main class.

Introduction

In a Personal Information Manager, users might want to manage a social calendar. To do this, you might define a class called Appointment to the information for a single event, and track information like the following:

    Starting and ending dates

    A description of the appointment

    A location for the appointment

    Attendees for the appointment

Naturally, this information is passed in by a user when he or she is setting up the appointment, so you define a constructor that allows you to set the state of a new Appointment object.

What exactly is needed to create an appointment, though? Different kinds of information are required depending on the specific type of the appointment. Some appointments might require a list of attendees (the monthly Monty Python film club meeting). Some might have start and end dates (JavaOne conference) and some might only have a single date—a plan to visit the art gallery for the M.C. Escher exhibit. When you consider these options, the task of creating an Appointment object is not trivial.

There are two possibilities for managing object creation, neither of them particularly attractive. You create constructors for every type of appointment you want to create, or you write an enormous constructor with a lot of functional logic. Each approach has its drawbacks—with multiple constructors, calling logic becomes more complex; with more functional logic built into the constructor, the code becomes more complex and harder to debug. Worse still, both approaches have the potential to cause problems if you later need to subclass Appointment.

Instead, delegate the responsibility of Appointment creation to a special AppointmentBuilder class, greatly simplifying the code for the Appointment itself. The AppointmentBuilder contains methods to create the parts of the Appointment, and you call the AppointmentBuilder methods that are relevant for the appointment type. Additionally, the AppointmentBuilder can ensure that the information passed in when creating the Appointment is valid, helping to enforce business rules. If you need to subclass Appointment, you either create a new builder or subclass the existing one. In either case, the task is easier than the alternative of managing object initialization through constructors.
Applicability

Use the Builder pattern when a class:

    Has complex internal structure (especially one with a variable set of related objects).

    Has attributes that depend on each other. One of the things a Builder can do is enforce staged construction of a complex object. This would be required when the Product attributes depend on one another. For instance, suppose you’re building an order. You might need to ensure that you have a state set before you move on to “building” the shipping method, because the state would impact the sales tax applied to the Order itself.

    Uses other objects in the system that might be difficult or inconvenient to obtain during creation.